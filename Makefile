
CC = gcc
AR = ar
DIR = $(shell pwd)
LIBDIR = lib
SQLITE_PATH = /usr/include
LIBSQLITE_PATH = /usr/lib/x86_64-linux-gnu
APP = $(LIBDIR)/libyesql.a 

CFLAGS = -std=c99 -I./include -I${SQLITE_PATH}

ifeq (${MODE},debug)
	CFLAGS += -O0 -g -DDEBUG -Wall -Wextra
else
	CFLAGS += -O2 
endif

ifeq (${LIB_TYPE},shared)
	CFLAGS += -fPIC
	APP = $(LIBDIR)/libyesql.so
endif

SOURCES=$(wildcard src/*.c)
OBJECTS=$(patsubst %.c, %.o, $(SOURCES))

TEST_SRC=$(wildcard test/*.c)
TESTS=$(patsubst %.c, %, $(TEST_SRC))

all: $(APP)

# // Recursively build object files
test/%.o : test/%.c
	$(CC) -c $(CFLAGS) $< -o $@

src/%.o : src/%.c
	$(CC) -c $(CFLAGS) $< -o $@

lib/libyesql.a : $(OBJECTS) | $(LIBDIR)
	$(AR) -rc $@ $^

lib/libyesql.so : $(OBJECTS) | $(LIBDIR)
	$(CC) -shared -Wall -o $@ $^ 	

$(LIBDIR):
	mkdir -p $(LIBDIR)

# // Test Stuff
test: $(TESTS)

LDFLAGS = -L ./$(LIBDIR) -lyesql -L ${LIBSQLITE_PATH} -lsqlite3 -lpthread -ldl 

$(TESTS): %: %.o
	$(CC) -DDEBUG -o $@ $^ $(LDFLAGS)

build: clean all test

clean:
	rm -f test/*.o src/*.o lib/* test/*.db $(TESTS)

install:
	export PATH=$(DIR):$$PATH

