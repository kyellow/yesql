
/* ye-sqlite tester for ye-database */

#include <assert.h>
#include <ye-db-table.h>

YeDatabase *tdb = NULL;

static void testCase1();
static void testCase2();
static void testCase3();

int
main(int argc, char **argv)
{
	// @TODO: Set Attributes
	tdb = yedb_open("./yetest.db", RUNMODE_HIGH_PERFORMANCE);

	assert( tdb != NULL );

	if( yedb_isOpen(tdb) <= 0 )
		log_dbg(" Sqlite error message : %s",yedb_getErrorMessage(tdb));

	testCase1();
	testCase2();
	testCase3();

	return yedb_close(tdb);
}

static void 
testCase1()
{
	// Case 1 : Create test table on yetest.db
	//
	YeDBColumnList * clist = yedb_column_createColumnList();
	assert( clist != NULL);

	YeDBColumn* c1 = yedb_column_create("id",0,COLUMN_TYPE_INT,COLUMN_CONSTRAINT_PRIMARY_KEY,NULL);
	assert ( c1 != NULL);
	assert ( yedb_column_addToColumnList(clist,c1) );

	YeDBColumn* c2 = yedb_column_create("name",0,COLUMN_TYPE_TEXT,COLUMN_CONSTRAINT_NOT_NULL,NULL);
	assert( c2 != NULL);
	assert( yedb_column_addToColumnList(clist,c2) );

	YeDBColumn* c3 = yedb_column_create("surname",0,COLUMN_TYPE_TEXT,COLUMN_CONSTRAINT_DEFAULT,NULL);
	assert( c3 != NULL);
	assert( yedb_column_addToColumnList(clist,c3) );

	YeDBColumn* c4 = yedb_column_create("age",0,COLUMN_TYPE_INT,COLUMN_CONSTRAINT_DEFAULT,NULL);
	assert( c4 != NULL);
	assert( yedb_column_addToColumnList(clist,c4) );

	YeDBColumn* c5 = yedb_column_create("salary",0,COLUMN_TYPE_REAL,COLUMN_CONSTRAINT_DEFAULT,NULL);
	assert( c5 != NULL);
	assert( yedb_column_addToColumnList(clist,c5) );

	YeDBTable * tbl1 = yedb_table_create(tdb, "test", clist);
	assert( tbl1 != NULL );
	printf("\n test table created on yetest.db ");

	yedb_table_print(tbl1);
	yedb_table_close(tbl1);
	
	assert( yedb_table_isExist(tdb,"test") > 0 );
	printf("\n test table is exist on yetest.db");

	assert( yedb_table_remove(tdb,"test") >= 0 );
	printf("\n test table removed on yetest.db ");

	assert( yedb_table_isExist(tdb,"test") <= 0 );
	printf("\n test table is not exist on yetest.db");
}

static void
testCase2()
{
	// COMMIT -> Transaction Control Test ( Depends ye-table )
	//
	YeDBColumnList * clist = yedb_column_createColumnList();
	assert( clist != NULL );

	YeDBColumn* c1 = yedb_column_create("id",0,COLUMN_TYPE_INT,COLUMN_CONSTRAINT_PRIMARY_KEY,NULL);
	assert ( c1 != NULL);
	assert ( yedb_column_addToColumnList(clist,c1) );

	YeDBColumn* c2 = yedb_column_create("valBool",0,COLUMN_TYPE_BOOL,COLUMN_CONSTRAINT_NOT_NULL,NULL);
	assert( c2 != NULL);
	assert( yedb_column_addToColumnList(clist,c2) );

	YeDBColumn* c3 = yedb_column_createFromDefinition("valDouble REAL");
	assert( c3 != NULL);
	assert( yedb_column_addToColumnList(clist,c3) );

	YeDBTable *tbl = yedb_table_create(tdb,"test2", clist);
	assert ( tbl != NULL );
	printf(" \n test2 table created on test.db ");

	yedb_table_print(tbl);
	/*
	YeDBRowList* rlist = yedb_row_createRowList();
	assert( rlist != NULL );
	*/
	assert ( yedb_transactionBegin(tdb) >= 0 );

	YeDBRow* r1 = yedb_row_create(clist,"0","1","1.1");
	assert( r1 != NULL );
	assert( yedb_table_addRow(tbl,r1) > 0 );

	YeDBRow* r2 = yedb_row_create(clist,"1","0","2.2");
	assert( r2 != NULL );
	assert( yedb_table_addRow(tbl,r2) > 0 );

	YeDBRow* r3 = yedb_row_create(clist,"2","0","3.3");
	assert( r3 != NULL );
	assert( yedb_table_addRow(tbl,r3) > 0 );

	assert ( yedb_transactionCommit(tdb) >= 0 );

	yedb_table_print(tbl);
	yedb_table_close(tbl);

}

static int testCB(int a, char **b, char **c)
{
	for( int i = 0; i < a; i++ )
	{
		printf("%s==%s\n",c[i], b[i] ? b[i] : "NULL" );
	}
	
	return 0;
}

static void
testCase3()
{

	YeDBColumnList * clist = yedb_column_createColumnList();
	assert( clist != NULL );

	YeDBColumn* c1 = yedb_column_create("id",0,COLUMN_TYPE_INT,COLUMN_CONSTRAINT_PRIMARY_KEY,NULL);
	assert ( c1 != NULL);
	assert ( yedb_column_addToColumnList(clist,c1) );

	YeDBColumn* c2 = yedb_column_create("valBool",0,COLUMN_TYPE_BOOL,COLUMN_CONSTRAINT_NOT_NULL,NULL);
	assert( c2 != NULL);
	assert( yedb_column_addToColumnList(clist,c2) );

	YeDBTable *tbl = yedb_table_create(tdb,"test3", clist);
	assert ( tbl != NULL );
	printf(" \n test2 table created on test.db ");

	yedb_table_print(tbl);
	
	yedb_execQuery(tdb,NULL,"INSERT INTO test3 VALUES(%s,%s)","2","1");
	yedb_execQuery(tdb,NULL,"INSERT INTO test3 VALUES(%s,%s)","5","0");

	yedb_table_print(tbl);

	yedb_execQuery(tdb,testCB,"SELECT * FROM %s","test3");
	
	yedb_table_print(tbl);
	yedb_table_close(tbl);
}
