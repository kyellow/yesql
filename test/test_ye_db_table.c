
/* ye-sqlite tester for ye-database */


#include <unistd.h>
#include <dirent.h>
#include <assert.h>
#include <ye-db-table.h>

static void
testDDL();

int main(int argc, char **argv)
{
	// --> execl("/usr/bin/wget","wget","https://github.com/cwoodruff/Chinook-Database/raw/master/databases/Chinook_Sqlite.sqlite","-O","sample.db",NULL);

	YeDatabase* testDB = yedb_open("./sample.db", RUNMODE_HIGH_PERFORMANCE);

	assert( testDB != NULL );

	// Case 1: Print Album Table
	YeDBTable * albumTable = yedb_table_open(testDB,"Album");

	assert( albumTable != NULL );
	yedb_table_print(albumTable);
	yedb_table_close(albumTable);

	// Case 2: Check "test" and "Album" table is exist
	
	assert( yedb_table_isExist(testDB,"Album") > 0 );
	printf("\n Album table is exist");

	assert( yedb_table_isExist(testDB,"test") == 0 );
	printf("\n test table is not exist");

	// Case 3 : Select query on sample db
	
	YeDBTable * artistTable = yedb_table_selectQuery(testDB,"Select * from %s","Artist");

	assert( artistTable != NULL );
	yedb_table_print(artistTable);
	yedb_table_close(artistTable);

	yedb_close(testDB);

	// TEST STUFF
	testDDL();

	return 0;
}

static void testDDL()
{
	// Open database on memory (without path)
	//
	YeDatabase* memdatabase = yedb_open("",RUNMODE_HIGH_ROBUSTNESS);

	assert( memdatabase != NULL );

	//
	YeDBColumnList * clist = yedb_column_createColumnList();
	assert( clist != NULL );

	YeDBColumn* c1 = yedb_column_createFromDefinition("key         INT       PRIMARY KEY");
	assert ( c1 != NULL);
	assert ( yedb_column_addToColumnList(clist,c1) );

	YeDBColumn* c2 = yedb_column_createFromDefinition("         vb BOOLEAN          NOT NULL    ");
	assert( c2 != NULL);
	assert( yedb_column_addToColumnList(clist,c2) );

	YeDBColumn* c3 = yedb_column_createFromDefinition(" vd REAL");
	assert( c3 != NULL);
	assert( yedb_column_addToColumnList(clist,c3) );

	YeDBTable *tbl = yedb_table_create(memdatabase,"tsttbl", clist);
	assert ( tbl != NULL );
	printf(" \n test2 table created on test.db ");

	yedb_table_print(tbl);

	// Add new column after created table

	YeDBColumn* c4 = yedb_column_createFromDefinition(" vi INTEGER          ");
	assert( c4 != NULL);
	assert( yedb_table_addColumn(tbl,c4) > 0 );

	yedb_table_print(tbl);

	assert ( yedb_transactionBegin(memdatabase) >= 0 );

	YeDBRow* r1 = yedb_row_create(clist,"0","1","1.1","9");
	assert( r1 != NULL );
	assert( yedb_table_addRow(tbl,r1) > 0 );

	YeDBRow* r2 = yedb_row_create(clist,"1","0","2.2","8");
	assert( r2 != NULL );
	assert( yedb_table_addRow(tbl,r2) > 0 );

	YeDBRow* r3 = yedb_row_create(clist,"2","0","3.3","7");
	assert( r3 != NULL );
	assert( yedb_table_addRow(tbl,r3) > 0 );

	assert ( yedb_transactionCommit(memdatabase) >= 0 );

	yedb_table_print(tbl);

	// Remove row in the table
	assert ( yedb_table_deleteRow(tbl,&r3) >= 0 );
	assert (r3 == NULL);

	yedb_table_print(tbl);

	// Change table name
	assert( yedb_table_rename(&tbl,"newtbl") > 0 );
	assert( strcmp(tbl->tablename,"newtbl") == 0 );

	yedb_table_print(tbl);

	assert( yedb_row_updateCell(r2, c4, "2") >= 0 );
	assert( yedb_table_updateRow(tbl,r2) >= 0);

	yedb_table_print(tbl);

	assert( yedb_table_updateCell(tbl,r1,c4,"1") >= 0);
	
	yedb_table_print(tbl);
	yedb_table_close(tbl);

	yedb_close(memdatabase);
}

