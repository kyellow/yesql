
/* ye-sqlite tester for sqlite3-connection */

#include <unistd.h>
#include <sqlite3_connection.h>

static void
check(int ret)
{
	if (ret < 0)
		exit(1);

	if (ret > 0)
		log_dbg("%d rows affected", ret);
}

static void
print_db(sqlite *db)
{
	char tmp[32];
	sqlite_stmt *stmt = sqlite_prep(db, "select a from test where id = ?");

	for (int i = 0; i <= 10; i++)
	{
		sprintf(tmp,"%d",i);
		printf("%d^2 = %d\n", i, sqlite_readint(stmt, 0, tmp));
	}

	sqlite_clear(stmt);
}

int
main(int argc, char **argv)
{
	char tmp[32];
	sqlite *db = sqlite_open("./test.db", SO_RDWRCR, 5000);

	check(sqlite_exec(db, "drop table if exists test"));
	check(sqlite_exec(db, "create table test (id int, a text)"));

	for (int i = 0; i < 10; i++)
	{
		sprintf(tmp,"%d",i*i);
		check(sqlite_exec(db, "insert into test values (%d, %Q)", i,tmp));
	}

	print_db(db);
	
	if (sqlite_save(db, "./backup.db", 0) == SQLITE_OK)
		printf("saved to ./backup.db\n");
	
	if (sqlite_load(db, "./backup.db", 0) == SQLITE_OK)
		printf("loaded from ./backup.db\n");
	
	print_db(db);
	
	sqlite_close(db);
	
	return 0;
}
