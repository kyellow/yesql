#ifndef _YE_DB_TABLE_H_
#define _YE_DB_TABLE_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <ye-database.h>
#include <ye-db-row.h>


/* Database Table Wrapper Structure */
typedef struct YeDBTable_{
	const YeDatabase *yedb;
	char *tablename;
	// ---
	YeDBRowList *rowlist;
	YeDBColumnList *collist;
}YeDBTable;

// Initial Stuff
YeDBTable *yedb_table_open(const YeDatabase * in_db, const char* in_tableName);  
int yedb_table_close(YeDBTable *);

// Check Database Table Stuff
int yedb_table_isExist(const YeDatabase* in_db, const char *in_tableName);
void yedb_table_print(const YeDBTable *);

/*
DDL Stuff

Data Definition Language (DDL) statements are used to define the database structure or schema. Some examples:

    CREATE - to create objects in the database
    ALTER - alters the structure of the database
    DROP - delete objects from the database
    TRUNCATE - remove all records from a table, including all spaces allocated for the records are removed
    COMMENT - add comments to the data dictionary
    RENAME - rename an object
*/

YeDBTable *yedb_table_create(const YeDatabase *in_db, const char* in_tableName, YeDBColumnList * in_collist);  
int yedb_table_addColumn( YeDBTable * in_tbl, YeDBColumn* in_newCol );
int yedb_table_remove( const YeDatabase *in_db, const char *in_tableName );
int yedb_table_truncate(const YeDatabase *in_db, const char *in_tableName );
int yedb_table_rename(YeDBTable **inout_tbl, const char* in_newname);

/*
DML Stuff

Data Manipulation Language (DML) statements are used for managing data within schema objects. Some examples:

    SELECT - retrieve data from the a database
    INSERT - insert data into a table
    UPDATE - updates existing data within a table
    DELETE - deletes all records from a table, the space for the records remain
    MERGE - UPSERT operation (insert or update)
    CALL - call a PL/SQL or Java subprogram
    EXPLAIN PLAN - explain access path to data
    LOCK TABLE - control concurrency
*/
YeDBTable* yedb_table_selectQuery(const YeDatabase *in_db, const char *in_format, ...);

int yedb_table_addRow(YeDBTable *, YeDBRow *);
int yedb_table_updateRow(YeDBTable *, YeDBRow *);
int yedb_table_updateCell(YeDBTable *, YeDBRow *, YeDBColumn*, const char *);
int yedb_table_deleteRow(YeDBTable *, YeDBRow **);

//////// Index Stuff
int yedb_table_createIndex(YeDBTable *,const char* in_indexName, ...);
int yedb_table_removeIndex(YeDBTable *,const char* in_indexName);

#if defined (__cplusplus)
}
#endif

#endif
