#ifndef _YE_DB_UTIL_H_
#define _YE_DB_UTIL_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifndef DEBUG
#define DEBUG 0
#endif

/// Log Util Macros

#define log_err(fmt, ...) \
        do { if (DEBUG) fprintf(stderr, "\n%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

#define log_dbg(fmt, ...) \
        do { if (DEBUG) fprintf(stdout, "\n%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

/// Memory Util Macros

#define SCMalloc(a) ({ \
    void *ptrmem = NULL; \
    \
    ptrmem = malloc((a)); \
    if (ptrmem == NULL) { \
		log_err("SCMalloc failed: %s, while trying " \
			"to allocate %ju bytes", strerror(errno), (uintmax_t)(a)); \
    } else \
		memset(ptrmem,0,a); \
	\
    (void*)ptrmem; \
})

#define SCRealloc(x, a) ({ \
    void *ptrmem = NULL; \
    \
    ptrmem = realloc((x), (a)); \
    if (ptrmem == NULL) { \
		log_err("SCRealloc failed: %s, while trying " \
			"to allocate %ju bytes", strerror(errno), (uintmax_t)(a)); \
    } \
    (void*)ptrmem; \
})

#define SCCalloc(nm, a) ({ \
    void *ptrmem = NULL; \
    \
    ptrmem = calloc((nm), (a)); \
    if (ptrmem == NULL) { \
		log_err("SCCalloc failed: %s, while trying " \
			"to allocate %ju bytes", strerror(errno), (uintmax_t)(a)); \
    } \
    (void*)ptrmem; \
})

#define SCStrdup(a) ({ \
    char *ptrmem = NULL; \
    \
    ptrmem = util_strdup((a)); \
    if (ptrmem == NULL) { \
		log_err("SCStrdup failed: %s, while trying " \
			"to allocate %ju bytes", strerror(errno), (uintmax_t)strlen((a))); \
    } \
    (void*)ptrmem; \
})

#define SCFree(a) ({ \
    free(a); \
	a = NULL; \
})

/// Safe String Functions

size_t
util_strlcpy(char *out_buf, const char* in_src, size_t sizebuf);

size_t
util_strlcat(char *out_buf, const char* in_src, size_t sizebuf);

size_t
util_strhash(const char *in_str);

char *
util_strdup(const char *in_str);
/// Double-Linked List Macro

//  List definitions.
 
#define	LIST_HEAD(name, type) \
typedef struct name { \
	struct type *lh_first;	/* first element */	\
	int count; \
}name

#define	LIST_HEAD_INITIALIZER(head)	\
	{ NULL }

#define	LIST_ENTRY(type)\
struct {\
	struct type *le_next;	/* next element */	\
	struct type **le_prev;	/* address of previous next element */	\
}

// List functions.
 
#define	LIST_INIT(head) do {	\
	(head)->lh_first = NULL;	\
	(head)->count = 0;			\
} while (/*CONSTCOND*/0)

#define	LIST_INSERT_AFTER(listelm, elm) do {	\
	if (((elm)->others.le_next = (listelm)->others.le_next) != NULL)	\
		(listelm)->others.le_next->others.le_prev =	\
		    &(elm)->others.le_next;	\
	(listelm)->others.le_next = (elm);	\
	(elm)->others.le_prev = &(listelm)->others.le_next;	\
} while (/*CONSTCOND*/0)

#define	LIST_INSERT_BEFORE(listelm, elm) do { \
	(elm)->others.le_prev = (listelm)->others.le_prev; \
	(elm)->others.le_next = (listelm); \
	*(listelm)->others.le_prev = (elm);	\
	(listelm)->others.le_prev = &(elm)->others.le_next; \
} while (/*CONSTCOND*/0)

#define	LIST_INSERT_HEAD(head, elm) do {		\
	if (((elm)->others.le_next = (head)->lh_first) != NULL)		\
		(head)->lh_first->others.le_prev = &(elm)->others.le_next;\
	(head)->lh_first = (elm);	\
	(elm)->others.le_prev = &(head)->lh_first;	\
} while (/*CONSTCOND*/0)

#define	LIST_REMOVE(elm) do { \
	if ((elm)->others.le_next != NULL)	\
		(elm)->others.le_next->others.le_prev = 	\
		    (elm)->others.le_prev;	\
	*(elm)->others.le_prev = (elm)->others.le_next;	\
} while (/*CONSTCOND*/0)

#define	LIST_FOREACH(var, head)	\
	for ((var) = ((head)->lh_first);	\
		(var);	\
		(var) = ((var)->others.le_next))

// List access methods.
 
#define	LIST_EMPTY(head)		((head)->lh_first == NULL)
#define	LIST_FIRST(head)		((head)->lh_first)
#define	LIST_NEXT(elm)			((elm)->others.le_next)
#define LIST_COUNT(head)		((head)->count)


#if defined (__cplusplus)
}
#endif

#endif


