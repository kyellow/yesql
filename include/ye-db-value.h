#ifndef _YE_DB_VALUE_H
#define _YE_DB_VALUE_H_

#if defined (__cplusplus)
extern "C" {
#endif

enum{
	TYPE_INT,
	TYPE_DOUBLE,
	TYPE_TEXT,
	TYPE_BLOB,
	TYPE_UNDEFINED,
	TYPE_BOOL,
	TYPE_NUMERIC,
	TYPE_DATE,
	TYPE_DATETIME
};

/* Database Cell Value Wrapper Structure */
typedef struct YeDBValue_{
	char* value;
	int type;
}YeDBValue;

// Initial Stuff
YeDBValue *yedb_value_create(const char * in_val, int type);
int yedb_value_delete(YeDBValue *);

/*  SQLite supports the standard SQL variables CURRENT_DATE, CURRENT_TIME, and CURRENT_TIMESTAMP  */

#if defined (__cplusplus)
}
#endif

#endif
