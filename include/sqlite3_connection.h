#ifndef _SQLITE3_CONNECTION_H_
#define _SQLITE3_CONNECTION_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <sqlite3.h>
#include <ye-db-util.h>

#define SO_RD				SQLITE_OPEN_READONLY
#define SO_RDWR				SQLITE_OPEN_READWRITE
#define SO_RDWRCR			(SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE)
#define SO_U				SQLITE_OPEN_URI
#define SO_NM				SQLITE_OPEN_NOMUTEX
#define SO_FM				SQLITE_OPEN_FULLMUTEX
#define SO_SC				SQLITE_OPEN_SHARED_CACHE
#define SO_PC				SQLITE_OPEN_PRIVATE_CACHE

/*Alias some sqlite metods*/
#define sqlite				sqlite3
#define sqlite_stmt			sqlite3_stmt
#define sqlite_value		sqlite3_value

#define sqlite_close		sqlite3_close
#define sqlite_reset		sqlite3_reset
#define sqlite_step			sqlite3_step
#define sqlite_clear		sqlite3_finalize
#define sqlite_errmsg		sqlite3_errmsg
#define sqlite_errcode		sqlite3_errcode
#define sqlite_rowid		sqlite3_last_insert_rowid
#define sqlite_fcount		sqlite3_column_count
#define sqlite_fname		sqlite3_column_name
#define sqlite_ftype		sqlite3_column_type
#define sqlite_cname		sqlite3_column_origin_name
#define sqlite_getdb		sqlite3_db_handle
#define sqlite_getsql		sqlite3_sql
#define sqlite_gettxt		sqlite3_column_text
#define sqlite_getstr		(const char *)sqlite_gettxt
#define sqlite_getlen		sqlite3_column_bytes
#define sqlite_getint		sqlite3_column_int
#define sqlite_geti64		sqlite3_column_int64
#define sqlite_getdbl		sqlite3_column_double
#define sqlite_getblb		sqlite3_column_blob
#define sqlite_getval		sqlite3_column_value
#define sqlite_putval		sqlite3_bind_value
#define sqlite_putint		sqlite3_bind_int
#define sqlite_putdbl		sqlite3_bind_double

#define sqlite_puttxt(stmt, col, txt)			sqlite3_bind_text((stmt), (col), (txt), -1, SQLITE_STATIC)
#define sqlite_putstr(stmt, col, str)			sqlite_puttxt((stmt), (col), (const char *)(str))
#define sqlite_putblb(stmt, col, buf, len)		sqlite3_bind_blob((stmt), (col), (buf), (len), SQLITE_STATIC)
#define sqlite_snprintf(buf, size, format, ...) sqlite3_snprintf((size), (buf), (format), ##__VA_ARGS__)
#define sqlite_exists(db, tbl, cnd_fmt, ...)	(sqlite_fetchstr((db), NULL, 0, ("select null from %s where " cnd_fmt), (tbl), ##__VA_ARGS__) == 1)
#define sqlite_truncate(db, tbl)				sqlite_exec((db), "delete from %s;", (tbl))
#define sqlite_removetable(db, tbl)				sqlite_exec((db), "drop table %s;", (tbl))
#define sqlite_renametable(db, name, newname)   sqlite_exec((db), "alter table %s rename to %s;",(name),(newname))


/*Transaction */
#define sqlite_begin(db)	sqlite_exec((db), "BEGIN TRANSACTION;")
#define sqlite_beginx(db)	sqlite_exec((db), "BEGIN EXCLUSIVE TRANSACTION;")
#define sqlite_commit(db)	sqlite_exec((db), "COMMIT TRANSACTION;")
#define sqlite_rollback(db)	sqlite_exec((db), "ROLLBACK TRANSACTION;")
#define sqlite_vacuum(db)	sqlite_exec((db), "vacuum");

/* Attach - Detach*/
#define sqlite_attach(db, path, db_name)	sqlite_exec((db), "ATTACH %s AS %s;",(path),(db_name))
#define sqlite_detach(db, db_name)	sqlite_exec((db), "DETACH %s;",(db_name))

/*Open */
sqlite *sqlite_open(const char *in_dbpath, int flags, int bto);

/*Statement */
sqlite_stmt *sqlite_prep(sqlite *in_db, const char *in_format, ...);

/*Bind */
int sqlite_bind(sqlite_stmt *in_stmt, ...);
int sqlite_bindv(sqlite_stmt *in_stmt, const char *in_params[]);

/*Exec */
int sqlite_exec(sqlite *in_db, const char *in_format, ...);
int sqlite_execv(sqlite *in_db, const char *in_format, ... /*, const char *in_params[]*/);
int sqlite_execp(sqlite *in_db, sqlite_stmt *in_stmt, ...);
int sqlite_execpv(sqlite *in_db, sqlite_stmt *in_stmt, const char *in_params[]);
sqlite_stmt *sqlite_execq(sqlite *in_db, const char *in_format, ...);
sqlite_stmt *sqlite_execqv(sqlite *in_db, const char *in_format, ... /*, const char *in_params[]*/);

/*Read */
const char *sqlite_readstr(sqlite_stmt *in_stmt, const char *defval, ...);
int sqlite_readint(sqlite_stmt *in_stmt, int defval, ...);
int64_t sqlite_readi64(sqlite_stmt *in_stmt, int64_t defval, ...);
double sqlite_readdbl(sqlite_stmt *in_stmt, double defval, ...);

/*Fetch */
int sqlite_fetchstr(sqlite *in_db, char *out_buf, size_t size, const char *in_format, ...);
int sqlite_fetchint(sqlite *in_db, int *out, const char *in_format, ...);
int sqlite_fetchi64(sqlite *in_db, int64_t *out, const char *in_format, ...);
int sqlite_fetchdbl(sqlite *in_db, double *out, const char *in_format, ...);

/*Row Metods*/
int sqlite_getrow(sqlite_stmt *in_stmt, const char *out_fields[], int maxnfields);

/*Backup */
int sqlite_load(sqlite *in_db, const char *in_path, int bto);
int sqlite_save(sqlite *in_db, const char *in_path, int bto);


#if defined (__cplusplus)
}
#endif

#endif
