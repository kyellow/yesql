#ifndef _YE_DB_COLUMN_H_
#define _YE_DB_COLUMN_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <ye-db-util.h>

enum{
	COLUMN_TYPE_INT			= 0x01,
	COLUMN_TYPE_REAL		= 0x02,
	COLUMN_TYPE_TEXT		= 0x03,
	COLUMN_TYPE_BLOB		= 0x04,
	COLUMN_TYPE_UNDEFINED	= 0x05,		// for compability sqlite data type
	COLUMN_TYPE_BOOL,
	COLUMN_TYPE_NUMERIC,
	COLUMN_TYPE_DATE,
	COLUMN_TYPE_DATETIME
};

enum{
	COLUMN_CONSTRAINT_DEFAULT		= 0x00,
	COLUMN_CONSTRAINT_PRIMARY_KEY	= 0x01,
	COLUMN_CONSTRAINT_NOT_NULL		= 0x02,
	COLUMN_CONSTRAINT_UNIQUE		= 0x04
};

/* Database Column Wrapper Structure */
typedef struct YeDBColumn{
	char* columnname;
	int index;
	int type;
	int flag;
	char* defaultvalue;
	// ---
	LIST_ENTRY(YeDBColumn) others;
}YeDBColumn;


// Initial Stuff
YeDBColumn* yedb_column_create(const char *in_name, int index, int type, int flag, const char* in_defval); 
int yedb_column_delete(YeDBColumn *);

YeDBColumn *yedb_column_createFromDefinition(const char *in_def);		

// Check Column Stuff
int yedb_column_getTypeFromStr(const char *);
const char* yedb_column_getStrFromType(int);

int yedb_column_getFlagFromStr(const char *);
const char* yedb_column_getStrFromFlag(int);

// Column List Definition Stuff
LIST_HEAD(YeDBColumnList, YeDBColumn);		// Database Column List Structure

YeDBColumnList* yedb_column_createColumnList();
int yedb_column_deleteColumnList(YeDBColumnList* in_cl);

int yedb_column_addToColumnList(YeDBColumnList* in_cl, YeDBColumn *in_c);
int yedb_column_removeToColumnList(YeDBColumnList* in_cl, YeDBColumn *in_c);

YeDBColumn *yedb_column_getPrimaryKeyColumn(YeDBColumnList* in_cl);

#if defined (__cplusplus)
}
#endif

#endif
