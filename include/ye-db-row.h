#ifndef _YE_DB_ROW_H
#define _YE_DB_ROW_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <ye-db-value.h>
#include <ye-db-column.h>

/* Database Row Wrapper Structure */
typedef struct YeDBRow{
	const YeDBColumnList *columnlist;
	YeDBValue *valarray;
	// ---
	LIST_ENTRY(YeDBRow) others;
}YeDBRow;

// Initial Stuff
YeDBRow *yedb_row_create(const YeDBColumnList *, ...);
int yedb_row_delete(YeDBRow *);

int yedb_row_updateCell(YeDBRow *,const YeDBColumn *, const char *);

// Row List Definition Stuff
LIST_HEAD(YeDBRowList, YeDBRow);		// Database Row List Structure

YeDBRowList* yedb_row_createRowList();
int yedb_row_deleteRowList(YeDBRowList *);

int yedb_row_addToRowList(YeDBRowList *, YeDBRow *);
int yedb_row_removeToRowList(YeDBRowList *, YeDBRow **);

#if defined (__cplusplus)
}
#endif

#endif
