#ifndef _YE_DATABASE_H_
#define _YE_DATABASE_H_

#if defined (__cplusplus)
extern "C" {
#endif

#include <sqlite3_connection.h>
#include <stdbool.h>

/*Database Attribute Structure*/
typedef struct YeDatabaseAttr_{
	
	enum{
		AUTO_VACUUM_MODE_NONE = 0x00,
		AUTO_VACUUM_MODE_FULL = 0x01,
		AUTO_VACUUM_MODE_INCREMENTAL = 0x02
	}auto_vacuum;	// auto vacumm status

	int busy_timeout;	// as milliseconds
	int cache_size;		// Default : -2000 (2048000 bytes)  -> Represents  N*1024

	enum{
		JOURNAL_MODE_DELETE,
		JOURNAL_MODE_TRUNCATE,
		JOURNAL_MODE_PERSIST,
		JOURNAL_MODE_MEMORY,
		JOURNAL_MODE_WAL,
		JOURNAL_MODE_OFF
	}journal_mode;		
		
	int journal_size_limit;		// Default(bytes) : -1 no limit

	enum{
		LOCKING_MODE_NORMAL,
		LOCKING_MODE_EXCLUSIVE
	}locking_mode;			// 

	int page_size;			// Default: 4096 bytes (512,65536)	-> value depending sector size
//	bool secure_delete;		// Default Off
	
	enum{
		SYNCHRONOUS_OFF = 0x00,
		SYNCHRONOUS_NORMAL = 0x01,
		SYNCHRONOUS_FULL = 0x02,
		SYNCHRONOUS_EXTRA = 0x03
	} synchronous;			// Depends Journal Mode and Default: NORMAL

	enum{
		TEMP_STORE_DEFAULT = 0x00,
		TEMP_STORE_FILE = 0x01,
		TEMP_STORE_MEMORY = 0x02
	}temp_store;	// temporary tables and indices are kept in 

}YeDatabaseAttr;

/* Database Wrapper Structure */
typedef struct YeDatabase_{
	sqlite *sdb;	// @TODO: Make a void pointer, we will use it for other databases.
	YeDatabaseAttr *attr;
}YeDatabase;

enum // built in mode
{
	RUNMODE_UNKNOWN = 0x00,
	RUNMODE_HIGH_PERFORMANCE,
	RUNMODE_HIGH_ROBUSTNESS
};

// Initial Stuff
YeDatabase *yedb_open(const char *in_dbfile, int run_mode);  
int yedb_close( YeDatabase *);

// Attributes Stuff
YeDatabaseAttr * yedb_createAttribute(int run_mode);
int yedb_changeAttribute(YeDatabase* in_db, YeDatabaseAttr* in_newAttr);
int yedb_removeAttribute(YeDatabaseAttr *in_attr);

// Check DB Stuff
int yedb_isOpen(const YeDatabase*);
const char* yedb_getErrorMessage(const YeDatabase*);

/*
TCL Stuff

Transaction Control (TCL) statements are used to manage the changes made by DML statements. It allows statements to be grouped together into logical transactions.

    COMMIT - save work done
    SAVEPOINT - identify a point in a transaction to which you can later roll back
    ROLLBACK - restore database to original since the last COMMIT
    SET TRANSACTION - Change transaction options like isolation level and what rollback segment to use
*/
int yedb_transactionBegin(const YeDatabase *);
int yedb_transactionCommit(const YeDatabase *);
int yedb_transactionRollback(const YeDatabase *);


typedef int (*yedb_callback)(int,char**,char**);

int yedb_execQuery(const YeDatabase*, yedb_callback, const char* in_queryFormat, ... );


#if defined (__cplusplus)
}
#endif

#endif
