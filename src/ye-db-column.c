
#include <ye-db-column.h>

///// Check Column Stuff
int
yedb_column_getTypeFromStr( const char * in_typeStr)
{
	int retval = COLUMN_TYPE_UNDEFINED;

	if( in_typeStr[0] != '\0' && strlen(in_typeStr) > 2)
	{
		if( !memcmp("BLOB", in_typeStr,4) )
			retval = COLUMN_TYPE_BLOB;
		else if( !memcmp("BOOLEAN", in_typeStr,7) )
			retval = COLUMN_TYPE_BOOL;
		else if( !memcmp("DATETIME", in_typeStr, 8) )
			retval = COLUMN_TYPE_DATETIME;
		else if( !memcmp("DATE", in_typeStr, 4) )
			retval = COLUMN_TYPE_DATE;
		else if( !memcmp("NUMERIC", in_typeStr, 7) || !memcmp("DECIMAL", in_typeStr, 7) )
			retval = COLUMN_TYPE_NUMERIC;
		else if( !memcmp("REAL", in_typeStr,4) || !memcmp("DOUBLE", in_typeStr,6) || !memcmp("FLOAT",in_typeStr,5) )
			retval = COLUMN_TYPE_REAL;
		else if( !memcmp("TEXT",in_typeStr,4) || !memcmp("CLOB",in_typeStr,4) || strstr(in_typeStr,"CHAR") != NULL )
			retval = COLUMN_TYPE_TEXT;
		else if( strstr(in_typeStr,"INT") != NULL )
			retval = COLUMN_TYPE_INT;
	}
	
	return retval;
}

const char *
yedb_column_getStrFromType(int type)
{
	switch(type)
	{
		default:
		case COLUMN_TYPE_UNDEFINED :
			return "";
		case COLUMN_TYPE_INT:
			return "INT";
		case COLUMN_TYPE_BLOB:
			return "BLOB";
		case COLUMN_TYPE_BOOL:
			return "BOOLEAN";
		case COLUMN_TYPE_TEXT:
			return "TEXT";
		case COLUMN_TYPE_REAL:
			return "REAL";
		case COLUMN_TYPE_NUMERIC:
			return "NUM";
		case COLUMN_TYPE_DATETIME:
			return "DATETIME";
		case COLUMN_TYPE_DATE:
			return "DATE";
	}
}

int
yedb_column_getFlagFromStr(const char *in_str)
{
	int flag = COLUMN_CONSTRAINT_DEFAULT;

	if( in_str[0] != '\0' && strlen(in_str) > 5)
	{
		if( !memcmp("PRIMARY KEY",in_str,11) )
			flag = COLUMN_CONSTRAINT_PRIMARY_KEY;
		else if( !memcmp("NOT NULL",in_str,8) )
			flag = COLUMN_CONSTRAINT_NOT_NULL;
		else if( !memcmp("UNIQUE",in_str,6) )
			flag = COLUMN_CONSTRAINT_UNIQUE;
	}

	return flag;
}

const char *
yedb_column_getStrFromFlag(int flag)
{
	switch(flag)
	{
		default:
		case COLUMN_CONSTRAINT_DEFAULT:
			return "";
		case COLUMN_CONSTRAINT_NOT_NULL:
			return "NOT NULL";
		case COLUMN_CONSTRAINT_UNIQUE:
			return "UNIQUE";
		case COLUMN_CONSTRAINT_PRIMARY_KEY:
			return "PRIMARY KEY";
	}
}

///// Initial Stuff

YeDBColumn *
yedb_column_create( const char* in_name, int index, int type, int flag, const char* in_defval)
{
	YeDBColumn* col = NULL;

	if( in_name[0] != '\0' )
	{
		size_t total_size = sizeof(YeDBColumn) + (strlen(in_name) + 1);

		if(in_defval)
			total_size += (strlen(in_defval) + 1);

		col = SCMalloc(total_size);

		if( col != NULL )
		{
			col->index = index;
			col->type = type;
			col->flag = flag;
			col->columnname = (char*) col + sizeof(YeDBColumn);
			util_strlcpy(col->columnname, in_name, strlen(in_name) + 1);

			if(in_defval)
			{
				col->defaultvalue = col->columnname + (strlen(in_name) + 1);
				util_strlcpy(col->defaultvalue, in_defval, strlen(in_defval) + 1);
			}
			else
				col->defaultvalue = NULL;
		}
	}

	return col;
}

int
yedb_column_delete( YeDBColumn *in_cl)
{
	int retval = -2;

	if( in_cl != NULL )
	{
		SCFree(in_cl);
		retval = 1;
	}

	return retval;
}

YeDBColumn * 
yedb_column_createFromDefinition(const char* in_def)
{
	// Sample Definition : AlbumId INTEGER NOT NULL

	YeDBColumn* col = NULL;

	if( in_def[0] != '\0' )
	{
		/// Find first word
		while(*in_def && *in_def == ' ')	// skip first space
			in_def++;

		/// Get column name
		const char *space = in_def;

		while(*space && *space != ' ')	// find space
			space++;

		int cname_size = space - in_def;

		if(cname_size && *space == ' ')
		{
			while(*space && *space == ' ')	//skip space before type word
				space++;

			const char *tstart = space;

			/// Get Column type	
			while(*space && *space != ' ')	// find space
				space++;

			int ctype_size = space - tstart;
			int ctype = yedb_column_getTypeFromStr(tstart);		

			if(ctype_size && ctype != COLUMN_TYPE_UNDEFINED)
			{
				while(*space && *space == ' ')	//skip space before type word
					space++;

				const char *cstart = space;
	
				/// Get Column Constraint	
				while(*space && *space != ' ')	// find space
					space++;

				// int cconst_size = space - cstart; //@ TODO : use after maintain
				int flag = yedb_column_getFlagFromStr(cstart);

				char *colname = SCMalloc(cname_size + 1);

				if( colname == NULL )
					return col;

				char* cn = colname;
				while(cname_size-- && *in_def)
					*cn++ = *in_def++;
			
				*cn = 0;

				col = yedb_column_create( colname, 0, ctype,flag ,NULL);

				SCFree(colname);
			}
		}
	}

	return col;
}

///// Column Definition Stuff:

YeDBColumnList*
yedb_column_createColumnList()
{
	YeDBColumnList *cl = SCMalloc(sizeof(YeDBColumnList));
	if(cl != NULL)
		LIST_INIT(cl);
	return cl;
}

int
yedb_column_deleteColumnList(YeDBColumnList *in_cl)
{
	int retval = -2;

	if(in_cl != NULL)
	{
		YeDBColumn *prev, *col = LIST_FIRST(in_cl);
		
		while( col != NULL)
		{
			prev = LIST_NEXT(col);
			yedb_column_delete(col);
			col = prev;
		}

		retval = LIST_COUNT(in_cl); 
		SCFree(in_cl);
	}

	return retval;
}


int 
yedb_column_addToColumnList(YeDBColumnList* in_cl, YeDBColumn *in_column)
{
	int index = 0;

	if( in_cl != NULL && in_column != NULL)
	{
		if(in_cl->count++ == 0)
		{
			LIST_INSERT_HEAD(in_cl,in_column);
		
			if( in_column->index == 0)
				in_column->index = LIST_COUNT(in_cl);
		}
		else
		{
			YeDBColumn *prev = in_cl->lh_first;
			
			while(prev) 
			{
				if( in_column->index != 0)
				{
					if(prev->index > in_column->index)
					{
						LIST_INSERT_BEFORE(prev,in_column);
						break;	
					}
				}
				
				if(prev->others.le_next == NULL)
				{
					LIST_INSERT_AFTER(prev,in_column);

					in_column->index = LIST_COUNT(in_cl);
					break;
				}
					
				prev = prev->others.le_next;
			}
		}

		index = LIST_COUNT(in_cl);
	}

	return index;
}

int
yedb_column_removeToColumnList(YeDBColumnList* in_cl, YeDBColumn* in_column)
{
	int index = 0;

	if( in_cl != NULL && !LIST_EMPTY(in_cl) && in_column != NULL )
	{
		index = in_column->index;
		// @TODO
	}

	return index;
}

YeDBColumn *
yedb_column_getPrimaryKeyColumn(YeDBColumnList *in_cl)
{
	YeDBColumn* pk = NULL;

	if( in_cl != NULL )
		LIST_FOREACH(pk, in_cl)
			if( pk->flag == COLUMN_CONSTRAINT_PRIMARY_KEY )
				break;

	return pk;
}
