
#include <ye-db-table.h>
#include <ye-db-util.h>

///// Initial Stuff

inline static int
yedb_table_initColumns(YeDBTable* in_tbl)		
{
	// Get Table Metadata
	sqlite_stmt* stmt = sqlite_prep(in_tbl->yedb->sdb,"PRAGMA table_info('%s')",in_tbl->tablename);
	int index = 0;

	if( stmt != NULL)
	{
		YeDBColumn *prev = NULL;  

		while( sqlite_step(stmt) == SQLITE_ROW )
		{
			index = sqlite_getint(stmt,0);		// cid
			const char* cname = sqlite_getstr(stmt,1);	// name
			const char* ctype = sqlite_getstr(stmt,2);  // type
			int flag = (sqlite_getint(stmt,3)) ? COLUMN_CONSTRAINT_NOT_NULL : COLUMN_CONSTRAINT_DEFAULT;		// notnull
			const char* cdval = sqlite_getstr(stmt,4);	// default value
			flag |= sqlite_getint(stmt,5);		// pk

			log_dbg(" table: %s\t column--> index:%d\t cname:%s\t ctype:%s\t flag:%d\t cdval:%d\t ", in_tbl->tablename, index, cname, ctype, flag, cdval );

			YeDBColumn* col = yedb_column_create(cname, index, yedb_column_getTypeFromStr(ctype), flag, cdval); 

			if( col != NULL)
			{
				if( index == 0) 
					LIST_INSERT_HEAD(in_tbl->collist,col);
				else if(prev != NULL)
					LIST_INSERT_AFTER(prev,col);

				prev = col;
			}
		}

		if(index > 0)
			LIST_COUNT(in_tbl->collist) = index;	// update index count
		else if( prev != NULL)
			yedb_column_delete(prev);
	
		sqlite_clear(stmt);
	}

	return index;
}

inline static int
yedb_table_fetchRows( YeDBTable* in_tbl, sqlite_stmt* in_stmt )
{
	int index = 0;
	YeDBRow *prev = NULL;  

	do{
		
		YeDBRow * row = yedb_row_create(in_tbl->collist,NULL);

		if ( row != NULL )
		{
			for( int i = 0; i < LIST_COUNT(in_tbl->collist); i++ )
				row->valarray[i].value = SCStrdup( sqlite_getstr(in_stmt,i) );

			if( index++ == 0 ) 
				LIST_INSERT_HEAD(in_tbl->rowlist, row);
			else
				LIST_INSERT_AFTER(prev, row);

			prev = row;
		}

	}while( sqlite_step(in_stmt) == SQLITE_ROW );

	LIST_COUNT(in_tbl->rowlist) = index;

	return index;
}

inline static int
yedb_table_initRows( YeDBTable *in_tbl)
{
	int retval = 0;

	sqlite_stmt* stmt = sqlite_prep(in_tbl->yedb->sdb, "SELECT * FROM %s", in_tbl->tablename);

	if( stmt != NULL && sqlite_step(stmt) == SQLITE_ROW)
	{
		retval = yedb_table_fetchRows( in_tbl, stmt);
		sqlite_clear(stmt);
	}

	return retval;
}

inline static YeDBTable *
yedb_table_initTable(const char *in_tableName)
{
	YeDBTable * table = NULL;
	
	size_t total_size =  (strlen(in_tableName) + 1) + sizeof(YeDBTable);
	table = SCMalloc(total_size);

	if( table != NULL )
	{
		table->tablename = (char*) table + sizeof(YeDBTable);
		util_strlcpy(table->tablename, in_tableName, strlen(in_tableName) + 1);
	}

	return table;
}

YeDBTable *
yedb_table_open(const YeDatabase *in_db, const char *in_tblName)
{
	YeDBTable * adbt = NULL;

	if( in_db != NULL && in_tblName[0] != '\0' )
	{
		adbt = yedb_table_initTable(in_tblName);
		if( adbt != NULL )
		{
			adbt->yedb = in_db;
			adbt->collist = yedb_column_createColumnList();
			adbt->rowlist = yedb_row_createRowList();

			if( adbt->collist != NULL && yedb_table_initColumns(adbt) && adbt->rowlist != NULL)	
				yedb_table_initRows(adbt);
			else
				SCFree(adbt);
		}
	}

	return adbt;
}

int 
yedb_table_close( YeDBTable* in_tbl)
{
	int retval = -2;

	if( in_tbl != NULL )
	{	
		// Free DB row list
		yedb_row_deleteRowList(in_tbl->rowlist);

		// Free DB column List
		yedb_column_deleteColumnList(in_tbl->collist);

		SCFree(in_tbl);
		retval = 1;
	}

	return retval;
}

///// Check DB Table Stuff

void
yedb_table_print( const YeDBTable* in_tbl)
{
	if( in_tbl != NULL)
	{
		printf("\n\n\tTable Name: %s\n",in_tbl->tablename);	

		// Columns
		YeDBColumn* c = NULL;
		LIST_FOREACH(c,in_tbl->collist)
		{
			printf("%s\t",c->columnname);
		}
		
		printf("\n");

		LIST_FOREACH(c,in_tbl->collist)
			printf("%.*s\t",strlen(c->columnname), "--------------------------------------------------");

		// Rows
		YeDBRow* r = NULL;
		LIST_FOREACH(r,in_tbl->rowlist)
		{
			printf("\n");
			for(int i = 0; i < LIST_COUNT(in_tbl->collist); i++)
			{
				printf("%s\t",r->valarray[i].value);
			}
		}
			
		printf("\n\n");
	}
}

int 
yedb_table_isExist(const YeDatabase *in_db, const char * in_tableName)
{
	int retval = -2;
	
	if( yedb_isOpen(in_db) && in_tableName[0] != '\0' )
	{
		sqlite_stmt* stmt = sqlite_prep(in_db->sdb,"PRAGMA table_info('%s')",in_tableName);

		if( stmt != NULL )
		{
			if( sqlite_step(stmt) == SQLITE_ROW )
				retval = 1;
			else
				retval = 0; 

			sqlite_clear(stmt);
		}
		else
			retval = -1;
	}

	return retval;
}

////// DML STUFF

static inline int 
yedb_table_initQueryColumn(YeDBTable* in_tbl, sqlite_stmt* in_stmt)
{
	int colcount = sqlite_fcount(in_stmt);
	LIST_COUNT(in_tbl->collist) = colcount;

	YeDBColumn * prev = NULL;

	for( int i = 0; i < colcount; i ++)
	{
		YeDBColumn * col = yedb_column_create(sqlite_fname(in_stmt,i), i, sqlite_ftype(in_stmt,i), COLUMN_CONSTRAINT_DEFAULT, NULL);

		if( col != NULL )
		{
			if( i == 0 )
				LIST_INSERT_HEAD(in_tbl->collist,col);
			else
				LIST_INSERT_AFTER(prev,col);

			prev = col;
		}
	}

	return colcount;
}

static inline YeDBTable*
yedb_table_vselectQuery(const YeDatabase *in_db, const char *sql)
{
	YeDBTable * tbl = NULL;
	sqlite_stmt *stmt;

	if(sqlite3_prepare_v2(in_db->sdb, sql, -1, &stmt, NULL) == SQLITE_OK)
	{
		if( sqlite_step(stmt) == SQLITE_ROW )
		{
			char queryhash[10];
			sprintf(queryhash,"%08x",util_strhash(sql));

			tbl = yedb_table_initTable(queryhash);

			if( tbl != NULL )
			{
				tbl->yedb = in_db;
				tbl->collist = yedb_column_createColumnList();
				tbl->rowlist = yedb_row_createRowList();

				/// init Columns	
				if(tbl->collist != NULL && tbl->rowlist != NULL )
				{
					yedb_table_initQueryColumn(tbl,stmt);
					/// Fetch rows
					yedb_table_fetchRows(tbl,stmt);
				}
			}	
		}

		sqlite_clear(stmt);
	}

	return tbl;
}

YeDBTable *
yedb_table_selectQuery(const YeDatabase * in_db, const char *in_format, ...)
{
	YeDBTable *tbl = NULL;
	
	if( yedb_isOpen(in_db) )
	{
		va_list argv;
		va_start(argv, in_format);

		char* sql = sqlite3_vmprintf(in_format,argv);

		if( sql != NULL)
		{
			tbl = yedb_table_vselectQuery(in_db, sql);
			sqlite3_free(sql);
		}

		va_end(argv);
	}

	return tbl;
}

int
yedb_table_addRow(YeDBTable *in_tbl, YeDBRow *in_row)
{
	int retval = -2;

	if(in_tbl != NULL && in_row != NULL)
	{
		retval = yedb_row_addToRowList(in_tbl->rowlist, in_row);

		if( retval >= 0)
		{
			YeDBColumn* col = NULL;
			size_t sizev = 0, sizec = 0;

			LIST_FOREACH( col, in_tbl->collist )
			{
				if( in_row->valarray[col->index-1].value != NULL)
				{
					sizev += strlen(in_row->valarray[col->index-1].value) + 1;
					sizec += strlen(col->columnname) + 1;
				}
			}

			if( sizev != 0)
			{
				char *sqlv = SCMalloc(sizev);
				if( sqlv == NULL)
					return retval;
				char *sqlc = SCMalloc(sizec);
				if(sqlc == NULL )
				{
					SCFree(sqlv);
					return retval;
				}

				char *sc = sqlc, *sv = sqlv;

				LIST_FOREACH( col, in_tbl->collist)
				{
					if( in_row->valarray[col->index-1].value != NULL)
					{
						if(*sqlv != 0)
							*sv++ = ',';

						if(*sqlc != 0)
							*sc++ = ',';
 
						char *val = in_row->valarray[col->index-1].value;
						while(*val)
							*sv++ = *val++;

						*sv = 0;

						char *cn = col->columnname;
						while(*cn)
							*sc++ = *cn++;

						*sc = 0;
					}
				}
				
				retval = sqlite_exec(in_tbl->yedb->sdb,
					"INSERT INTO %s(%s) VALUES(%s);",in_tbl->tablename,sqlc,sqlv);			
				if( retval != SQLITE_OK)
					log_dbg("INSERT INTO -> table: %s , sqlc: %s, sqlv %s",in_tbl->tablename, sqlc,sqlv);

				SCFree(sqlv);
				SCFree(sqlc);
			}
		}
	}

	return retval;
}

int
yedb_table_updateRow(YeDBTable *in_tbl, YeDBRow *in_row)
{
	int retval = -2;

	if( in_tbl != NULL && in_row != NULL )
	{
		retval = -1;

		YeDBRow *rc;
		LIST_FOREACH(rc,in_tbl->rowlist)		// check row in tbl
		{
			if( in_row == rc)
			{
				size_t total_size = 0;

				YeDBColumn* pk = NULL, *val = NULL;
				LIST_FOREACH(val,in_tbl->collist)
				{
					if( val->flag == COLUMN_CONSTRAINT_PRIMARY_KEY )
						pk = val;
	
					if(rc->valarray[val->index-1].value != NULL)
						total_size += strlen(val->columnname) + 1 /*=*/ + strlen(rc->valarray[val->index-1].value);
				}

				if( total_size != 0 && pk != NULL)
				{
					total_size += LIST_COUNT(in_tbl->collist);	// add comma
		
					char* sqlc = SCMalloc(total_size);

					if( sqlc == NULL )
						return -3;

					char *sof = sqlc;
					LIST_FOREACH(val,in_tbl->collist)
					{
						if(rc->valarray[val->index-1].value != NULL)
						{
							if(*sqlc != 0)
								*sof++ = ',';

							char* cof = val->columnname;

							while(*cof)		// copy column name
								*sof++ = *cof++;

							*sof++ = '=';
							cof = rc->valarray[val->index-1].value;

							while(*cof)		// copy value
								*sof++ = *cof++;

							*sof = 0;
						}
					}
					
					retval = sqlite_exec(in_tbl->yedb->sdb,
					"UPDATE %s SET %s WHERE %s=%s;",in_tbl->tablename,sqlc,pk->columnname,rc->valarray[pk->index-1].value);
	
					SCFree(sqlc);
				}
				break;
			}
		}
	}
	
	return retval;
}

int
yedb_table_deleteRow(YeDBTable *in_tbl, YeDBRow ** in_row)
{
	int retval = -2;
	
	if( in_tbl != NULL && in_row != NULL && *in_row != NULL)
	{
		retval = -1;

		YeDBRow *rc;
		LIST_FOREACH(rc,in_tbl->rowlist)
		{
			if( *in_row == rc)
			{
				YeDBColumn* pk = yedb_column_getPrimaryKeyColumn(in_tbl->collist);

				if( pk != NULL )
				{
					retval = sqlite_exec(in_tbl->yedb->sdb,
						"DELETE FROM %s WHERE %s=%s;",in_tbl->tablename,pk->columnname,rc->valarray[pk->index-1].value);

					yedb_row_removeToRowList(in_tbl->rowlist,in_row);
				}
				break;
			}
		}

	}

	return retval;
}

int
yedb_table_updateCell( YeDBTable *in_tbl, YeDBRow *in_row, YeDBColumn *in_col, const char *in_val)
{
	int retval = -2;
	
	if( in_tbl != NULL && in_row != NULL && in_col != NULL && in_val != NULL)
	{
		retval = -1;

		YeDBColumn* pk = yedb_column_getPrimaryKeyColumn(in_tbl->collist);
			
		if( pk != NULL && in_col != pk )
		{
			sqlite_exec(in_tbl->yedb->sdb, "UPDATE %s SET %s=%s WHERE %s=%s;",in_tbl->tablename,in_col->columnname,in_val,pk->columnname,in_row->valarray[pk->index-1].value);

			retval = yedb_row_updateCell(in_row,in_col,in_val);
		}
	}

	return retval;
}

///// DDL Stuff

YeDBTable *
yedb_table_create( const YeDatabase *in_db, const char *in_tableName, YeDBColumnList* in_collist)
{
	YeDBTable *tbl = NULL;
	
	if( yedb_isOpen(in_db) && in_tableName[0] != '\0' && in_collist != NULL && !LIST_EMPTY(in_collist) )
	{
		tbl = yedb_table_initTable(in_tableName);
		
		if(tbl != NULL)
		{
			tbl->yedb = in_db;
			tbl->collist = in_collist;
			tbl->rowlist = yedb_row_createRowList();

			size_t total_size = LIST_COUNT(in_collist)*50;
			
			char *sql = SCMalloc(total_size);	// @WARNING: Attention there is limit here! 

			if( sql == NULL )
			{
				yedb_table_close(tbl);
				return NULL;
			}

			char comma = ' ';

			YeDBColumn * col = NULL;
			char *ss = sql;
	
			LIST_FOREACH( col, in_collist)
			{
				total_size = strlen(col->columnname) + 5; // with 2 spaces, comma, null

				const char *ctype = yedb_column_getStrFromType(col->type);
				if(ctype)
					total_size += strlen(ctype);

				const char *cflag = yedb_column_getStrFromFlag(col->flag);
				if(cflag)
					total_size += strlen(cflag);

				char *coldef = SCMalloc(total_size);

				if( coldef == NULL )
				{
					yedb_table_close(tbl);
					SCFree(sql);
					return NULL;
				}

				sprintf(coldef,"%c%s %s %s",comma, col->columnname, ctype, cflag);

				char* cc = coldef;

				while(*cc)
					*ss++ = *cc++;
				
				*ss = 0;

				SCFree(coldef);

				comma = ',';
			}

			if(sqlite_exec(in_db->sdb,"CREATE TABLE %s(%s);",in_tableName,sql) != SQLITE_OK)
				log_dbg("Create Table -> table: %s , field: %s ",in_tableName, sql);

			SCFree(sql);
		}
	}

	return tbl;
}

int
yedb_table_addColumn( YeDBTable * in_tbl, YeDBColumn *in_nc)
{
	int retval = -2;
	
	if( in_tbl != NULL && yedb_table_isExist(in_tbl->yedb, in_tbl->tablename) > 0 )
	{
		if( in_nc != NULL && in_nc->columnname != NULL )
		{
			retval = sqlite_exec(in_tbl->yedb->sdb,"ALTER TABLE %s ADD COLUMN %s %s %s;",in_tbl->tablename, in_nc->columnname, yedb_column_getStrFromType(in_nc->type), yedb_column_getStrFromFlag(in_nc->flag));

			if( retval == SQLITE_OK)
			{
				in_nc->index = 0;
				retval = yedb_column_addToColumnList(in_tbl->collist,in_nc);
			}
		}
		else
			retval = -1;
	}
	return retval;
}

int
yedb_table_truncate( const YeDatabase *in_db, const char *in_tableName)
{
	if( yedb_table_isExist(in_db,in_tableName) > 0 )
		return sqlite_truncate(in_db->sdb,in_tableName);
	else
		return -1;
}

int
yedb_table_remove( const YeDatabase *in_db, const char *in_tableName)
{
	if( yedb_table_isExist(in_db,in_tableName) > 0 )
		return sqlite_removetable(in_db->sdb,in_tableName);
	else
		return -1;
}

int 
yedb_table_rename( YeDBTable **inout_tbl, const char *in_tblName)
{
	int retval  = -2;
	if( inout_tbl != NULL)
	{
		YeDBTable* tbl = *inout_tbl;

		if( tbl != NULL && yedb_table_isExist(tbl->yedb, tbl->tablename) > 0 )
		{
			if(in_tblName != NULL)
			{
				retval = sqlite_renametable(tbl->yedb->sdb, tbl->tablename, in_tblName);	
				YeDBTable* newTable = yedb_table_initTable(in_tblName);

				if(newTable != NULL)
				{
					newTable->yedb = tbl->yedb;
					newTable->collist = tbl->collist;
					newTable->rowlist = tbl->rowlist;

					*inout_tbl = newTable;

					SCFree(tbl);	// remove old table name

					retval = 1;
				}
			}
			else
				retval = -1;
		}
	}
	return retval;
}

////// Index Stuff
int
yedb_table_removeIndex( YeDBTable * in_tbl, const char *in_iname)
{
	int retval = -2;
	
	if( in_tbl != NULL && yedb_table_isExist(in_tbl->yedb, in_tbl->tablename) > 0 )
	{
		if(in_iname != NULL)
			retval = sqlite_exec(in_tbl->yedb->sdb,"DROP INDEX IF EXISTS %s;",in_iname);
		else
			retval = -1;
	}

	return retval;
}

int 
yedb_table_createIndex(YeDBTable *in_tbl, const char * in_iname, ... )
{
	int retval = -2;
	
	if( in_tbl != NULL && yedb_table_isExist(in_tbl->yedb, in_tbl->tablename) > 0 )
	{
		if(in_iname != NULL && in_tbl->collist != NULL)
		{
			va_list args;
			va_start(args, in_iname);

			YeDBColumn* vlist[LIST_COUNT(in_tbl->collist)];

			size_t total_size = 0;

			// Calculate columnnames size with comma (Create index last parameter) 
			for( int i = 0; i <  LIST_COUNT(in_tbl->collist); i++)
			{
				vlist[i] = va_arg(args, YeDBColumn*);

				if( vlist[i] != NULL && vlist[i]->columnname != NULL)	
					total_size += strlen(vlist[i]->columnname);
				else
					break;
			}

			if( total_size > 0)
			{
				total_size += sizeof(vlist);		// with comma and last null

				char *cnames = SCMalloc(total_size);

				if(cnames == NULL)
				{
					va_end(args);
					return -3;
				}

				char* cns = cnames;

				for( unsigned int i = 0; i < sizeof(vlist); i++)
				{
					if(vlist[i] != NULL)
					{
						if(i != 0)
							*cns++ = ',';	// add comma among column names

						char *ss = vlist[i]->columnname;

						while(*ss)
							*cns++ = *ss++;

						*cns = 0;
					}
					else
						break;
				}

				retval = sqlite_exec(in_tbl->yedb->sdb,"CREATE INDEX %s ON %s(%s);",in_iname,in_tbl->tablename,cnames);

				SCFree(cnames);
			}

			va_end(args);
		}
		else
			retval = -1;
	}

	return retval;
}
