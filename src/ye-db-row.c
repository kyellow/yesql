
#include <ye-db-row.h>

YeDBRow *
yedb_row_create(const YeDBColumnList* in_cl, ...)
{
	YeDBRow * adbr = NULL;
	
	if( in_cl != NULL )
	{
		size_t total_size = sizeof(YeDBRow) + sizeof(YeDBValue) * in_cl->count;
		adbr = SCMalloc(total_size);

		if( adbr != NULL )
		{
			adbr->columnlist = in_cl;
			adbr->valarray = (YeDBValue*)((char*)adbr + sizeof(YeDBRow));
			
			va_list argv;
			va_start(argv,in_cl);
			const char *str = NULL;
			YeDBColumn* colmn = NULL;

			LIST_FOREACH(colmn, in_cl)
			{
				adbr->valarray[colmn->index-1].type = colmn->type;

				if(colmn->index == 1 || str != NULL)
				{
					str = va_arg(argv, const char *);
	
					if( str != NULL )	
						adbr->valarray[colmn->index-1].value = SCStrdup(str);
				}
			}

			va_end(argv);
		}
	}

	return adbr;
}

int
yedb_row_delete( YeDBRow * in_r)
{
	int retval = -2;
	
	if( in_r != NULL )
	{
		if( in_r->columnlist != NULL)
		{
			// first remove value array 
			for( int i = 0; i < LIST_COUNT(in_r->columnlist); i++)
				if( in_r->valarray[i].value != NULL)
					SCFree(in_r->valarray[i].value);
		}

		SCFree(in_r);
		retval = 1;
	}

	return retval;
}

int
yedb_row_updateCell(YeDBRow *in_row, const YeDBColumn *in_col, const char *in_val)
{
	int retval = -2;

	if( in_row != NULL && in_col != NULL && in_val != NULL && in_val != NULL)
	{
		retval = -1;

		YeDBColumn * clm =NULL;
		LIST_FOREACH(clm,in_row->columnlist)
		{
			if( clm == in_col)
			{
				if(in_row->valarray[clm->index-1].value != NULL)
					 SCFree(in_row->valarray[clm->index-1].value);

				in_row->valarray[clm->index-1].value = SCStrdup(in_val);
				retval = 1;
				break;
			}
		}
	}

	return retval;
}
///// Row Defibition Stuff

YeDBRowList* 
yedb_row_createRowList()
{
	YeDBRowList *rl = SCMalloc(sizeof(YeDBRowList));
	if(rl != NULL)
		LIST_INIT(rl);
	return rl;
}

int
yedb_row_deleteRowList(YeDBRowList *in_rl)
{
	int retval = -2;

	if( in_rl != NULL)
	{
		YeDBRow *prev, *row = LIST_FIRST(in_rl);
		
		while( row != NULL)
		{
			prev = LIST_NEXT(row);
			yedb_row_delete(row);
			row = prev;
		}

		retval = LIST_COUNT(in_rl); 
		SCFree(in_rl);
	}

	return retval;
}

int 
yedb_row_addToRowList( YeDBRowList *in_rl, YeDBRow *in_row)
{
	int index = 0;

	if( in_rl != NULL && in_row != NULL)
	{
		if(in_rl->count++ == 0)
		{
			LIST_INSERT_HEAD(in_rl,in_row);
		}
		else
		{
			YeDBRow *prev = in_rl->lh_first;
			
			while(prev) 
			{
				if(prev->others.le_next == NULL)
				{
					LIST_INSERT_AFTER(prev,in_row);
					break;
				}
				else	
					prev = prev->others.le_next;
			}
		}

		index = LIST_COUNT(in_rl);
	}
	return index;
}

int
yedb_row_removeToRowList( YeDBRowList *in_rl, YeDBRow ** inout_row)
{
	int retval = -2;

	if( in_rl != NULL && !LIST_EMPTY(in_rl) && inout_row != NULL && *inout_row != NULL)
	{
		retval = -1;

		YeDBRow* row = NULL;
		LIST_FOREACH(row,in_rl)	// check row in the list
		{
			if( row == *inout_row )
			{
				LIST_REMOVE(row);
				yedb_row_delete(row);
				retval = --LIST_COUNT(in_rl);
				*inout_row = NULL;
				break;
			}
		}
	}
	
	return retval;
}
