#include <sqlite3_connection.h>

#include <unistd.h>

//////////////// Open Metods ////////////////  

sqlite *
sqlite_open(const char *in_dbpath, int flags, int bto)
{
	sqlite3 *db = NULL;

	if (SQLITE_OK != sqlite3_open_v2(in_dbpath, &db, flags, NULL))
	{
		log_err("cannot open '%s': %s", in_dbpath, sqlite3_errmsg(db));
		sqlite3_close(db);
		return NULL;
	}

	log_dbg("%s opened", in_dbpath);

	if (SQLITE_OK != sqlite3_busy_timeout(db, bto))
	{
		log_err("cannot set busy timeout for '%s': %s", in_dbpath, sqlite3_errmsg(db));
		sqlite3_close(db);
		return NULL;
	}

	log_dbg("busy timeout set to %d msec for '%s'", bto,in_dbpath);

	if (SQLITE_OK != sqlite3_extended_result_codes(db, 1))
		log_dbg("cannot enable option extended result codes for '%s': %s", in_dbpath, sqlite3_errmsg(db));

	return db;
}

//////////////// Prepare Statement ////////////////  

inline static sqlite_stmt *
sqlite_vprep(sqlite *in_db, const char *in_format, va_list argv)
{
	char *zSql;
	sqlite3_stmt *stmt;

	if (in_db == NULL)
		return NULL;

	if ((zSql = sqlite3_vmprintf(in_format, argv)) == NULL)
	{
		log_err("sqlite3_vmprintf(\"%s\", ...) error", in_format);
		return NULL;
	}

	log_dbg("preparing \"%s\"", zSql);

	if (SQLITE_OK != sqlite3_prepare_v2(in_db, zSql, -1, &stmt, NULL))
	{
		log_err("prepare \"%s\": %s", zSql, sqlite3_errmsg(in_db));
		sqlite3_free(zSql);
		return NULL;
	}

	sqlite3_free(zSql);
	return stmt;
}

sqlite_stmt *
sqlite_prep(sqlite *in_db, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);
	va_end(argv);

	return stmt;
}


////////////////  Bind Metods ////////////////  

inline static int
sqlite_vbind(sqlite_stmt *in_stmt, va_list argv)
{
	if (in_stmt == NULL) return -1;

	if (SQLITE_OK != sqlite3_reset(in_stmt))
	{
		log_err("reset: %s", sqlite3_errmsg(sqlite3_db_handle(in_stmt)));
		return -1;
	}

	int nParams = sqlite3_bind_parameter_count(in_stmt);

	for (int i = 0; i < nParams; i++)
	{
		const char *param = va_arg(argv, const char *);

		if (SQLITE_OK != sqlite3_bind_text(in_stmt, i + 1, param, -1, SQLITE_STATIC))
		{
			log_err("binding text '%s' to param %d: %s", param, i + 1,
					sqlite3_errmsg(sqlite3_db_handle(in_stmt)));
			return -1;
		}
	}

	return 0;
}

int
sqlite_bindv(sqlite_stmt *in_stmt, const char *in_params[])
{
	if (in_stmt == NULL) return -1;

	if (SQLITE_OK != sqlite3_reset(in_stmt))
	{
		log_err("reset %s", sqlite3_errmsg(sqlite3_db_handle(in_stmt)));
		return -1;
	}

	int nParams = sqlite3_bind_parameter_count(in_stmt);

	for (int i = 0; i < nParams; i++)
	{
		if (SQLITE_OK != sqlite3_bind_text(in_stmt, i + 1, in_params[i], -1, SQLITE_STATIC))
		{
			log_err("binding text '%s' to param %d: %s", in_params[i], i + 1,
					sqlite3_errmsg(sqlite3_db_handle(in_stmt)));
			return -1;
		}
	}

	return 0;
}

int
sqlite_bind(sqlite_stmt *in_stmt, ...)
{
	va_list argv;
	va_start(argv, in_stmt);
	int ret = sqlite_vbind(in_stmt, argv);
	va_end(argv);

	return ret;
}

//////////////// Exec Metods //////////////// 

inline static int
sqlite_vexecp(sqlite *in_db, sqlite_stmt *in_stmt, va_list argv)
{
	if (sqlite_vbind(in_stmt, argv) == -1)
		return -1;

	if (SQLITE_DONE != sqlite_step(in_stmt))
	{
		log_err("execute \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(in_db));
		return -1;
	}

	return sqlite3_changes(in_db);
}

int
sqlite_execp(sqlite *in_db, sqlite_stmt *in_stmt, ...)
{
	va_list argv;
	va_start(argv, in_stmt);
	int ret = sqlite_vexecp(in_db, in_stmt, argv);
	va_end(argv);

	return ret;
}

int
sqlite_execpv(sqlite *in_db, sqlite_stmt *in_stmt, const char *in_params[])
{
	if (sqlite_bindv(in_stmt, in_params) == -1)
		return -1;

	if (SQLITE_DONE != sqlite_step(in_stmt))
	{
		log_err("execute \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(in_db));
		return -1;
	}

	return sqlite3_changes(in_db);
}

inline static int
sqlite_vexecv(sqlite *in_db, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);
	int ret = sqlite_execpv(in_db, stmt, va_arg(argv, const char **));
	sqlite_clear(stmt);

	return ret;
}

int
sqlite_execv(sqlite *in_db, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vexecv(in_db, in_format, argv);
	va_end(argv);

	return ret;
}

inline static int
sqlite_vexec(sqlite *in_db, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);
	int ret = sqlite_vexecp(in_db, stmt, argv);
	sqlite_clear(stmt);

	return ret;
}

int
sqlite_exec(sqlite *in_db, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vexec(in_db, in_format, argv);
	va_end(argv);

	return ret;
}

inline static sqlite_stmt *
sqlite_vexecq(sqlite *in_db, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_vbind(stmt, argv) == -1)
	{
		sqlite_clear(stmt);
		return NULL;
	}

	return stmt;
}

sqlite_stmt *
sqlite_execq(sqlite *in_db, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	sqlite_stmt *stmt = sqlite_vexecq(in_db, in_format, argv);
	va_end(argv);

	return stmt;
}

inline static sqlite_stmt *
sqlite_vexecqv(sqlite *in_db, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_bindv(stmt, va_arg(argv, const char **)) == -1)
	{
		sqlite_clear(stmt);
		return NULL;
	}

	return stmt;
}

sqlite_stmt *
sqlite_execqv(sqlite *in_db, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	sqlite_stmt *stmt = sqlite_vexecqv(in_db, in_format, argv);
	va_end(argv);

	return stmt;
}
//////////////// Read Metods //////////////// 

inline static const char *
sqlite_vreadstr(sqlite_stmt *in_stmt, const char *defval, va_list argv)
{
	if (sqlite_vbind(in_stmt, argv) == -1)
		return defval;
	
	switch (sqlite_step(in_stmt))
	{
		default:
			log_err("query \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(sqlite_getdb(in_stmt)));
		case SQLITE_DONE:
			return defval;
		case SQLITE_ROW:
			return sqlite_getstr(in_stmt, 0);
	}
}

const char *
sqlite_readstr(sqlite_stmt *in_stmt, const char *defval, ...)
{
	va_list argv;
	va_start(argv, defval);
	const char *ret = sqlite_vreadstr(in_stmt, defval, argv);
	va_end(argv);

	return ret;
}

inline static int
sqlite_vreadint(sqlite_stmt *in_stmt, int defval, va_list argv)
{
	if (sqlite_vbind(in_stmt, argv) == -1)
		return defval;

	switch (sqlite_step(in_stmt))
	{
		default:
			log_err("query \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(sqlite_getdb(in_stmt)));
		case SQLITE_DONE:
			return defval;
		case SQLITE_ROW:
			return sqlite_getint(in_stmt, 0);
	}
}

int
sqlite_readint(sqlite_stmt *in_stmt, int defval, ...)
{
	va_list argv;
	va_start(argv, defval);
	int ret = sqlite_vreadint(in_stmt, defval, argv);
	va_end(argv);

	return ret;
}

inline static int64_t
sqlite_vreadi64(sqlite_stmt *in_stmt, int64_t defval, va_list argv)
{
	if (sqlite_vbind(in_stmt, argv) == -1)
		return defval;

	switch (sqlite_step(in_stmt))
	{
		default:
			log_err("query \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(sqlite_getdb(in_stmt)));
		case SQLITE_DONE:
			return defval;
		case SQLITE_ROW:
			return sqlite_geti64(in_stmt, 0);
	}
}

int64_t
sqlite_readi64(sqlite_stmt *in_stmt, int64_t defval, ...)
{
	va_list argv;
	va_start(argv, defval);
	int64_t ret = sqlite_vreadi64(in_stmt, defval, argv);
	va_end(argv);

	return ret;
}

inline static double
sqlite_vreaddbl(sqlite_stmt *in_stmt, double defval, va_list argv)
{
	if (sqlite_vbind(in_stmt, argv) == -1)
		return defval;

	switch (sqlite_step(in_stmt))
	{
		default:
			log_err("query \"%s\": %s", sqlite_getsql(in_stmt), sqlite_errmsg(sqlite_getdb(in_stmt)));
		case SQLITE_DONE:
			return defval;
		case SQLITE_ROW:
			return sqlite_getdbl(in_stmt, 0);
	}
}

double
sqlite_readdbl(sqlite_stmt *in_stmt, double defval, ...)
{
	va_list argv;
	va_start(argv, defval);
	double ret = sqlite_vreaddbl(in_stmt, defval, argv);
	va_end(argv);

	return ret;
}

//////////////// Fetch Metods //////////////// 

inline static int
sqlite_vfetchstr(sqlite *in_db, char *out_buf, size_t size, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_vbind(stmt, argv) == -1)
		return -1;

	switch (sqlite_step(stmt))
	{
		case SQLITE_ROW:
			util_strlcpy(out_buf, sqlite_getstr(stmt, 0), size);
			sqlite_clear(stmt);
			return 1;
		case SQLITE_DONE:
			sqlite_clear(stmt);
			return 0;
		default:
			log_err("query \"%s\": %s", sqlite_getsql(stmt), sqlite_errmsg(in_db));
			sqlite_clear(stmt);
			return -1;
	}
}

int
sqlite_fetchstr(sqlite *in_db, char *out_buf, size_t size, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vfetchstr(in_db, out_buf, size, in_format, argv);
	va_end(argv);

	return ret;
}

inline static int
sqlite_vfetchint(sqlite *in_db, int *out, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_vbind(stmt, argv) == -1)
		return -1;

	switch (sqlite_step(stmt))
	{
		case SQLITE_ROW:
			*out = sqlite_getint(stmt, 0);
			sqlite_clear(stmt);
			return 1;
		case SQLITE_DONE:
			sqlite_clear(stmt);
			return 0;
		default:
			log_err("query \"%s\": %s", sqlite_getsql(stmt), sqlite_errmsg(in_db));
			sqlite_clear(stmt);
			return -1;
	}
}

int
sqlite_fetchint(sqlite *in_db, int *out, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vfetchint(in_db, out, in_format, argv);
	va_end(argv);

	return ret;
}

inline static int
sqlite_vfetchi64(sqlite *in_db, int64_t *out, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_vbind(stmt, argv) == -1)
		return -1;

	switch (sqlite_step(stmt))
	{
		case SQLITE_ROW:
			*out = sqlite_geti64(stmt, 0);
			sqlite_clear(stmt);
			return 1;
		case SQLITE_DONE:
			sqlite_clear(stmt);
			return 0;
		default:
			log_err("query \"%s\": %s", sqlite_getsql(stmt), sqlite_errmsg(in_db));
			sqlite_clear(stmt);
			return -1;
	}
}

int
sqlite_fetchi64(sqlite *in_db, int64_t *out, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vfetchi64(in_db, out, in_format, argv);
	va_end(argv);

	return ret;
}

inline static int
sqlite_vfetchdbl(sqlite *in_db, double *out, const char *in_format, va_list argv)
{
	sqlite_stmt *stmt = sqlite_vprep(in_db, in_format, argv);

	if (sqlite_vbind(stmt, argv) == -1)
		return -1;

	switch (sqlite_step(stmt))
	{
		case SQLITE_ROW:
			*out = sqlite_getdbl(stmt, 0);
			sqlite_clear(stmt);
			return 1;
		case SQLITE_DONE:
			sqlite_clear(stmt);
			return 0;
		default:
			log_err("query \"%s\": %s", sqlite_getsql(stmt), sqlite_errmsg(in_db));
			sqlite_clear(stmt);
			return -1;
	}
}

int
sqlite_fetchdbl(sqlite *in_db, double *out, const char *in_format, ...)
{
	va_list argv;
	va_start(argv, in_format);
	int ret = sqlite_vfetchdbl(in_db, out, in_format, argv);
	va_end(argv);

	return ret;
}

//////////////// Row Metods //////////////// 

int
sqlite_getrow(sqlite_stmt *in_stmt, const char *out_fields[], int maxnfields)
{
	int i, nCols = sqlite_fcount(in_stmt);

	if (maxnfields < nCols)
		nCols = maxnfields;

	for (i = 0; i < nCols; i++)
		out_fields[i] = sqlite_getstr(in_stmt, i);

	return nCols;
}

//////////////// Backup Metods //////////////// 

int 
sqlite_load(sqlite *in_db, const char *in_path, int bto)
{
	sqlite *fromDb = sqlite_open(in_path, SO_RD, bto);
	sqlite3_backup *bak = sqlite3_backup_init(in_db, "main", fromDb, "main");
	int ret = -1;

	if (bak)
	{
		sqlite3_backup_step(bak, -1);
		ret = sqlite3_backup_finish(bak);
	}

	sqlite_close(fromDb);
	
	return ret;
}

int 
sqlite_save(sqlite *in_db, const char *in_path, int bto)
{
	sqlite *toDb = sqlite_open(in_path, SO_RDWRCR, bto);
	sqlite3_backup *bak = sqlite3_backup_init(toDb, "main", in_db, "main");
	int ret = -1;
	
	if (bak)
	{
		sqlite3_backup_step(bak, -1);
		ret = sqlite3_backup_finish(bak);
	}

	sqlite_close(toDb);
	
	return ret;
}
