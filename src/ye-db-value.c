#include <ye-db-value.h>
#include <ye-db-util.h>

YeDBValue *
yedb_value_create(const char * in_val, int type)
{
	YeDBValue* adbv = NULL;

	size_t total_size = sizeof(YeDBValue);

	if( in_val[0] != '\0' )
		total_size = (strlen(in_val) + 1) + sizeof(YeDBValue);

	adbv = SCMalloc(total_size);

	if( adbv != NULL)
	{
		adbv->type = type;

		if( in_val[0] != '\0' )
		{
			adbv->value = (char *) adbv + sizeof(YeDBValue);
			util_strlcpy(adbv->value, in_val, strlen(in_val) + 1);
		}
		else
		{
			adbv->value = NULL;
		}
	}

	return adbv;
}

int 
yedb_value_delete( YeDBValue *in_dbval)
{
	int retval = -2;

	if( in_dbval != NULL )
	{
		SCFree(in_dbval);
		retval = 1;
	}
	return retval;
}
