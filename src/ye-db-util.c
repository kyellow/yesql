
#include <ye-db-util.h>

#include <stdlib.h>

size_t
util_strlcpy(char *out_buf, const char* in_src, size_t sizebuf) // safe copy
{
	size_t len = sizebuf;

	if (!len)
		return 0;

	if (!in_src)
		return *out_buf = 0;

	while (--len && *in_src)
		*out_buf++ = *in_src++;

	*out_buf = 0;

	if (!len && *in_src)
		return sizebuf;

	return sizebuf - len - 1;
}


size_t
util_strlcat(char *out_buf, const char* in_src, size_t sizebuf) // safe cat
{
	size_t len = sizebuf;

	while (len && *out_buf)
		len--, out_buf++;

	if (!len)
		return sizebuf;

	if (!in_src)
		return sizebuf - len;

	while (--len && *in_src)
		*out_buf++ = *in_src++;

	*out_buf = 0;

	if (!len && *in_src)
		return sizebuf;

	return sizebuf - len - 1;
}

size_t
util_strhash(const char *in_str)
{
	size_t h = 5381;

	while (*in_str) {
		h += (h << 5);
		h ^= *in_str++;
	}

	return h;
}

char *
util_strdup(const char * in_str)
{
	size_t len = strlen(in_str) + 1;
	char * tmp = SCMalloc(len);

	if( tmp != NULL)
		util_strlcpy(tmp, in_str, len);

	return tmp;	
}
