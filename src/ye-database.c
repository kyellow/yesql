#include <ye-database.h>

// Forward Decleration 
inline static void yedb_performAttribute(YeDatabase *in_db, bool avoid_defval);
///// initial stuff

YeDatabase *
yedb_open( const char *in_dbfile, int run_mode)
{
	YeDatabase * adb = NULL;

	adb = SCMalloc(sizeof(YeDatabase));

	if( adb != NULL)
	{
		// @TODO: before set attributes
		//
		if( in_dbfile == NULL )
			adb->sdb = sqlite_open(":memory:",SO_RDWRCR,0);
		else
			adb->sdb = sqlite_open(in_dbfile,SO_RDWRCR,0);
	
		if( adb->sdb == NULL)
		{
			SCFree(adb);
		}
		else 
		{
			adb->attr = yedb_createAttribute(run_mode);
			yedb_performAttribute(adb,true);
		}
	}

	return adb;
}

int 
yedb_close( YeDatabase * in_db)
{
	int retval = -2;

	if( in_db != NULL )
	{
		retval = sqlite_close(in_db->sdb);
		yedb_removeAttribute(in_db->attr);
		SCFree(in_db);
	}
	
	return retval;
}

///// check stuff

int
yedb_isOpen(const YeDatabase * in_db)
{
	if( in_db == NULL || in_db->sdb == NULL)
		return 0;
	else
		return 1;
}

const char*
yedb_getErrorMessage(const YeDatabase *in_db)
{
	if( in_db != NULL && in_db->sdb != NULL)
		return sqlite_errmsg(in_db->sdb);
	else
		return NULL;
}

///// TCL Stuff

int
yedb_transactionBegin( const YeDatabase *in_db)
{
	if( yedb_isOpen(in_db) )
		return sqlite_begin(in_db->sdb);
	else
		return -1;
}

int
yedb_transactionCommit( const YeDatabase *in_db)
{
	if( yedb_isOpen(in_db) )
		return sqlite_commit(in_db->sdb);
	else
		return -1;
}

int
yedb_transactionRollback( const YeDatabase *in_db)
{
	if( yedb_isOpen(in_db) )
		return sqlite_rollback(in_db->sdb);
	else
		return -1;
}


/////////// Attributes Stuff
int 
yedb_removeAttribute(YeDatabaseAttr * in_at)
{
	int retval = -2;
	
	if(in_at != NULL)
	{
		SCFree(in_at);
		retval = 1;
	}

	return retval;
}

YeDatabaseAttr* 
yedb_createAttribute(int run_mode)
{
	YeDatabaseAttr *attr = NULL;

	if( run_mode != RUNMODE_UNKNOWN )
	{
		attr = SCMalloc(sizeof(YeDatabaseAttr));
		
		if(attr != NULL)
		{
			switch(run_mode)
			{
				default:
					attr->auto_vacuum = AUTO_VACUUM_MODE_NONE;
					attr->busy_timeout = 0;
					attr->cache_size = -2000;
					attr->journal_mode = JOURNAL_MODE_WAL;
					attr->journal_size_limit = -1;
					attr->locking_mode = LOCKING_MODE_NORMAL;
					attr->page_size = 4096;
					attr->synchronous = SYNCHRONOUS_NORMAL;
					attr->temp_store = TEMP_STORE_DEFAULT;
					break;
				case RUNMODE_HIGH_PERFORMANCE:
					attr->auto_vacuum = AUTO_VACUUM_MODE_NONE;
					attr->busy_timeout = 0;
					attr->cache_size = 2000;
					attr->journal_mode = JOURNAL_MODE_OFF;
					attr->journal_size_limit = -1;
					attr->locking_mode = LOCKING_MODE_EXCLUSIVE;	// Exclusive access to DB to avoid lock/unlock for each transaction
					attr->page_size = 4096;
					attr->synchronous = SYNCHRONOUS_OFF;
					attr->temp_store = TEMP_STORE_MEMORY;
					break;
				case RUNMODE_HIGH_ROBUSTNESS:
					attr->auto_vacuum = AUTO_VACUUM_MODE_NONE;
					attr->busy_timeout = 0;
					attr->cache_size = -2000;
					attr->journal_mode = JOURNAL_MODE_WAL;
					attr->journal_size_limit = -1;					
					attr->locking_mode = LOCKING_MODE_NORMAL;
					attr->page_size = 4096;
					attr->synchronous = SYNCHRONOUS_FULL;
					attr->temp_store = TEMP_STORE_DEFAULT;
					break;
			}
		}
	}

	return attr;
}

int
yedb_changeAttribute( YeDatabase * in_db, YeDatabaseAttr* in_newAttr)
{
	int retval = -2;

	if( in_newAttr != NULL && in_db != NULL)
	{
		yedb_removeAttribute(in_db->attr);
		in_db->attr = in_newAttr;
		yedb_performAttribute(in_db,false);
	}

	return retval;
}

inline static void
yedb_performAttribute(YeDatabase *in_db, bool avoid_defval)
{
	YeDatabaseAttr * at = in_db->attr;

	if( at != NULL )
	{
		if( !avoid_defval || at->auto_vacuum != AUTO_VACUUM_MODE_NONE )
			sqlite_exec(in_db->sdb,"PRAGMA auto_vacuum=%d",at->auto_vacuum);

		if( !avoid_defval || at->busy_timeout != 0 )
			sqlite_exec(in_db->sdb,"PRAGMA busy_timeout=%d",at->busy_timeout);

		if( !avoid_defval || at->cache_size != -2000 )
			sqlite_exec(in_db->sdb,"PRAGMA cache_size=%d",at->cache_size);

		switch( at->journal_mode )
		{
			default:
			case JOURNAL_MODE_WAL :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=WAL"); break;
			case JOURNAL_MODE_OFF :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=OFF"); break;
			case JOURNAL_MODE_MEMORY :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=MEMORY"); break;
			case JOURNAL_MODE_DELETE :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=DELETE"); break;
			case JOURNAL_MODE_TRUNCATE :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=TRUNCATE"); break;
			case JOURNAL_MODE_PERSIST :
				sqlite_exec(in_db->sdb,"PRAGMA journal_mode=PERSIST"); break;
		}

		if( !avoid_defval || at->journal_size_limit != -1 )
			sqlite_exec(in_db->sdb,"PRAGMA journal_size_limit=%d",at->journal_size_limit);

		if( !avoid_defval || at->locking_mode != LOCKING_MODE_NORMAL )
			sqlite_exec(in_db->sdb,"PRAGMA locking_mode=EXCLUSIVE");

		if( !avoid_defval || at->page_size != 4096 )
			sqlite_exec(in_db->sdb,"PRAGMA page_size=%d",at->page_size);

		if( !avoid_defval || at->synchronous != SYNCHRONOUS_NORMAL )
			sqlite_exec(in_db->sdb,"PRAGMA synchronous=%d",at->synchronous);

		if( !avoid_defval || at->temp_store !=  TEMP_STORE_DEFAULT)
			sqlite_exec(in_db->sdb,"PRAGMA temp_store=%d",at->temp_store);
	}
}

/// HElper metods


static int
yedb_callback_wrap( void* cb, int argc, char **argv, char ** azColName )
{
	yedb_callback rcb = (yedb_callback)cb;
	return rcb(argc, argv, azColName);
}

int
yedb_execQuery( const YeDatabase* in_db, yedb_callback ac, const char* in_queryFormat, ...)
{

	if( yedb_isOpen(in_db) )
	{
		va_list argv;
		va_start(argv,in_queryFormat);

		char *sql;

		if( (sql = sqlite3_vmprintf(in_queryFormat, argv)) == NULL)
		{
			va_end(argv);
			return -1;
		}

		int retval = 0;

		char *errmsg;
		if( SQLITE_OK != sqlite3_exec(in_db->sdb, sql, (ac != NULL) ? yedb_callback_wrap : NULL , ac, &errmsg) )
		{
			log_err(" yedb_execQuery : %s : %s ",sql, errmsg);
			sqlite3_free(errmsg);
			retval = -1;
		}

		sqlite3_free(sql);
		va_end(argv);

		return retval;
	}
	
	return -2;
}
